libhtml-html5-sanity-perl (0.105-5) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper dependency to >= 9, since that's what is used in
    debian/compat.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 11 Jul 2022 17:44:37 +0100

libhtml-html5-sanity-perl (0.105-4) unstable; urgency=medium

  * Simplify rules: Stop resolve build-dependencies in rules file.
  * Wrap and sort control file.
  * Use short-form dh sequencer (not cdbs).
    Stop build-depend on cdbs.
  * Stop build-depend on dh-buildinfo.
  * Declare compliance with Debian Policy 4.2.1.
  * Update copyright info: Extend coverage of packaging.
  * Update watch file: Fix typo in usage comment.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 Nov 2018 21:16:01 +0100

libhtml-html5-sanity-perl (0.105-3) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/*: update URLs from {search,www}.cpan.org to MetaCPAN.
  * debian/*: update GitHub URLs to use HTTPS.
  * Add (build) dependency on liblocale-codes-perl.
    Thanks to Niko Tyni for the bug report. (Closes: #903299)
  * Declare compliance with Debian Policy 4.1.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Jul 2018 17:10:26 +0200

libhtml-html5-sanity-perl (0.105-2) unstable; urgency=medium

  * Fix enhance (not recommend) libhtml-html5-parser-perl.
    Thanks to Kjetil Kjernsmo.
  * Modernize Vcs-* fields:
    + Use https protocol.
    + Use anonscm hostname.
    + Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Use debhelper compatibility level 9 (not 7).
  * Update git-buildpackage config: Filter any .git* files.
  * Update watch file:
    + Use format version 4.
    + Watch MetaCPAN URL.
    + Mention gbp --uscan in usage comment.
    + Tighten version regex.
  * Modernize cdbs:
    + Drop upstream-tarball hints: Use gbp import-orig --uscan.
    + Do copyright-check in maintainer script (not during build).
      Relax to build-depend unversioned on cdbs.
      Stop build-depend on devscripts.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
    + Fix double-space-delimit copyright holders.
    + Extend coverage for myself. Relicense packaging as GPL-3+.
    + Add alternate git source URL.
    + List CPAN issue tracker as preferred contact.
  * Add lintian overrides regarding License-Reference.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 01 Sep 2017 00:37:09 +0200

libhtml-html5-sanity-perl (0.105-1) unstable; urgency=medium

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 0.105
  * Update copyright years, dropping inc/ paragraphs removed upstream
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able
  * Update vcs fields to their canonical value

 -- Florian Schlichting <fsfs@debian.org>  Fri, 09 Oct 2015 23:32:21 +0200

libhtml-html5-sanity-perl (0.104-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Jonas Smedegaard ]
  * Add README.source emphasizing control.in file as *not* a show-stopper for
    contributions, referring to wiki page for details.

  [ Florian Schlichting ]
  * Import Upstream version 0.104
  * Remove copyright paragraph for deleted inc/Scalar/Util*
  * Bump copyright years
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0)
  * Remove build-dependency on libobject-authority-perl
  * Remove versions from build-dependencies satisfied in oldstable

 -- Florian Schlichting <fsfs@debian.org>  Sat, 25 May 2013 20:49:06 +0200

libhtml-html5-sanity-perl (0.103-1) unstable; urgency=low

  [ Florian Schlichting ]
  * Imported Upstream version 0.103
  * Dropped 2001_disable_verification.patch, modified test was removed.
  * Bumped copyright years, adjusted DEP-5 headers and added/deleted stanzas
    for new and removed files under inc/.
  * Updated (build-)dependencies.
  * Added myself to Uploaders and copyright.
  * Fixed copyright-refers-to-symlink-license lintian warning.

  [ Jonas Smedegaard ]
  * Update copyright file: Improve references for convenience copy of
    Module::Install.

  [ gregor herrmann ]
  * Remove debian/source/local-options; abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Sat, 17 Dec 2011 16:48:32 +0100

libhtml-html5-sanity-perl (0.101-2) unstable; urgency=low

  * Improve package relations:
    + Stop declaring packages part of core Perl even in oldstable:
      - libtest-simple-perl
  * Update copyright file:
    + Rewrite using draft 174 of DEP-5 format.
    + Rewrap license fields at 72 chars, and shorten comments.
  * Bump policy compliance to standards-version 3.9.2.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 08 May 2011 20:32:53 +0200

libhtml-html5-sanity-perl (0.101-1) unstable; urgency=low

  * Initial packaging release.
    Closes: bug#616521.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 05 Mar 2011 09:14:15 +0100
